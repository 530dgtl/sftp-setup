#SFTP-Setup

* Start with an Ubuntu 16.04 server
* SSH into the server
* `git clone` this repo
* `cd` into the directory
* Update lines 8 & 9 of the sftpSetup.sh file to match your users
* run `chmod +x` on the sftpSetup.sh file
* run `./sftpSetup.sh`
* run `sudo passwd <username>` for each user to set their passwords
* You're done, connect to your SFTP server with one of your users
