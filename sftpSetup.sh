#!/bin/bash

# creating the sftp group
sudo addgroup sftp

# creating two users
# edit to create the users you need
sudo useradd -c "My User" -M -g sftp myuser
sudo useradd -c "Other User" -M -g sftp other

# creating jailed directory
sudo mkdir /var/sftp
sudo chown root:root /var/sftp
sudo chmod 755 /var/sftp

# creating transfers directory
sudo mkdir /var/sftp/transfers
sudo chown myuser:sftp /var/sftp/transfers
sudo chgrp -R sftp /var/sftp/transfers
sudo chmod -R 775 /var/sftp/transfers

# copying config file into temp file for modification
sudo cp /etc/ssh/sshd_config temp.txt
sudo chown ubuntu:ubuntu temp.txt
sudo chmod 777 temp.txt

# adding to config settings
echo 'Match Group sftp' >> temp.txt
echo 'ForceCommand internal-sftp' >> temp.txt
echo 'PasswordAuthentication yes' >> temp.txt
echo 'ChrootDirectory /var/sftp' >> temp.txt
echo 'PermitTunnel no' >> temp.txt
echo 'AllowAgentForwarding no' >> temp.txt
echo 'AllowTcpForwarding no' >> temp.txt
echo 'X11Forwarding no' >> temp.txt

# moving config file to where it lives
sudo mv -f temp.txt /etc/ssh/sshd_config

# fixing permissions on file
sudo chmod 644 /etc/ssh/sshd_config
sudo chown root:root /etc/ssh/sshd_config

# restarting sshd
sudo systemctl restart sshd
